# web_recolor

- [Web Recolor](#orgff65161)
  - [Testing](#org7d0570b)
  - [Configuration](#org552656a)


<a id="orgff65161"></a>

# Web Recolor

A simple Firefox Add-In for recolorizing web pages. The project is currently in a very early stage and more a proof of concept than a finished product, therefore changes of the colors needed to be made directly in source code.


<a id="org7d0570b"></a>

## Testing

Currently the project only can be tested. A convenient way to do that is through using [web-ext](https://www.npmjs.com/package/web-ext)

```bash
git clone
cd web_recolor
web-ext run
```


<a id="org552656a"></a>

## Configuration

The colors can be changed in **recolor.js** beginning at line 56 to 71, the preset color palette is based on [Kanagawa](https://github.com/rebelot/kanagawa.nvim)

```js
var light_red    = "#e82424";
var dark_red     = "#c34043";
var light_green  = "#98bb6c";
var dark_green   = "#76946a";
var light_blue   = "#7fb4ca";
var dark_blue    = "#7e9cd8";
var light_yellow = "#e6c384";
var dark_yellow  = "#c0a36e";
var light_orange = "#ffb05c";
var dark_orange  = "#ffb05c";
var light_mauve  = "#938aa9";
var dark_mauve   = "#957fb8";
var light_pink   = "#77546c";
var dark_pink    = "#77546c";
var light_gray   = "#ffffff";
var dark_gray    = "#2d4f67";
```
