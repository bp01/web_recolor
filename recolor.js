function addCss(rule) {
  let css = document.createElement('style');
  css.type = 'text/css';
  if (css.styleSheet) css.styleSheet.cssText = rule;
  else css.appendChild(document.createTextNode(rule));
  document.getElementsByTagName("head")[0].appendChild(css);
}

// CSS rules
//let rule  = 'a {background-color: red !important;}';
//    rule += '.blue {background-color: blue}';

// Load the rules && execute after the DOM loads
//window.onload = function() {addCss(rule)};
//document.getElementsByTagName("a").style.backgroundColor = "green";

function RGBToHex(rgb) {
  let sep = rgb.indexOf(",") > -1 ? "," : " ";

  rgb = rgb.substr(4).split(")")[0].split(sep);

  let r = (+rgb[0]).toString(16),
      g = (+rgb[1]).toString(16),
      b = (+rgb[2]).toString(16);

  if (r.length == 1)
    r = "0" + r;
  if (g.length == 1)
    g = "0" + g;
  if (b.length == 1)
    b = "0" + b;

  return "#" + r + g + b;
}

function get_rgb(rgb) {
  let sep = rgb.indexOf(",") > -1 ? "," : " ";

  rgb = rgb.substr(4).split(")")[0].split(sep);

  let r = (+rgb[0]),
      g = (+rgb[1]),
      b = (+rgb[2]);

  return { r, g, b };
}

function get_hue(rgb) {
  let {r, g, b} = get_rgb(rgb); 
  if ((r * 0.2126) + (g * 0.7152) + (b * 0.0722) < (255 / 2)) { return true; }
  else { return false; } 
}

function unify_color(rgb, invert) {

 var light_red    = "#e82424"; 
 var dark_red     = "#c34043"; 
 var light_green  = "#98bb6c"; 
 var dark_green   = "#76946a"; 
 var light_blue   = "#7fb4ca"; 
 var dark_blue    = "#7e9cd8"; 
 var light_yellow = "#e6c384"; 
 var dark_yellow  = "#c0a36e"; 
 var light_orange = "#ffb05c"; 
 var dark_orange  = "#ffb05c"; 
 var light_mauve  = "#938aa9"; 
 var dark_mauve   = "#957fb8"; 
 var light_pink   = "#77546c"; 
 var dark_pink    = "#77546c"; 
 var light_gray   = "#ffffff"; 
 var dark_gray    = "#2d4f67"; 

  let {r, g, b} = get_rgb(rgb); 
  var base = get_hue(rgb);
  var is_dark;

  if(base == true && invert == true) {
    is_dark = false;
    //is_dark = true;
  }
  else if(base == false && invert == false) {
    is_dark = false;
    //is_dark = true;
  }
  else if(base == false && invert == true) {
    is_dark = true;
    //is_dark = false;
  }
  else if(base == true && invert == false) {
    is_dark = true;
    //is_dark = false;
  }

  const values = [r, g, b];
  values.sort(function(a, b){return a - b}); // a - b ascending; b - a descending

  if ((((values[2] + values[1])/2) + 15) >= values[0] &&  (((values[2] + values[1])/2) - 15) <= values[0]) {
    if (is_dark)  { return dark_gray; }
    else          { return light_gray; }
  }
  if (r > g && r > b) {
    if (g > b && g <= 145) {
      if(is_dark) { return dark_orange; }
      else        { return light_orange;}
    }
    else if(g > b && g > 145) {
      if(is_dark) { return dark_yellow;}
      else        { return light_yellow;}
    }
    else if(b > g && b <= 100) {
      if(is_dark) { return dark_pink;}
      else        { return light_pink;}
    }
    else {
      if(is_dark) { return dark_red;}
      else        { return light_red;}
    }
  }
  else if (g > b && g > r) {
    if(is_dark)   { return dark_green;}
    else          { return light_green;}
  } 
  else {
    if(r > g) {
      if(is_dark) { return dark_mauve;}
      else        { return light_mauve;}
    }
    else if(g > r) {
      //return light_blue;
      if (is_dark) {return dark_gray;}
      else {return light_blue;}
    }
    else {
      if(is_dark) { return dark_blue;}
      else {return light_blue;}
    }
  }
}

function get_parent(base) {
  let parent = base.parentElement; 
  let style = getComputedStyle(parent);
  if(style.backgroundColor == "rgba(0, 0, 0, 0)") {
    get_parent(parent);
  }
  else {
    return parent;
  }
}

function adjust_to_background_color(element) {
  let style = getComputedStyle(element);
  //let parent_style = getComputedStyle(get_parent(element));
  //let parent = get_parent(element);

  //if(get_hue(style.color) == get_hue(parent_style.backgroundColor)) {
  //   element.style.cssText = "color: #ff0000 !important";
  //}
  //else {
     element.style.cssText = "color: #00ff00 !important";
  //}
}

function get_children(element) {
  var dark_bg; 
  for (let i = 0; i < element.children.length; i++) {
    let style = getComputedStyle(element.children[i]);

    console.log(element.children[i].tagName + ":" + style.backgroundColor + " = " + RGBToHex(style.color) + "==>" + unify_color(style.color, false));

    if (style.color != "rgba(0, 0, 0, 0)") {
      var invert;
      if      (get_hue(style.color) == false && dark_bg == true) { invert = false; element.children[i].style.cssText = "color: " + unify_color(style.color, false) + "!important"; }
      else if (get_hue(style.color) == false && dark_bg == false){ invert = true;   element.children[i].style.cssText = "color: " + unify_color(style.color, true) + "!important";} /*{ invert = true;   }*/ 
      else if (get_hue(style.color) == true && dark_bg == false) { invert = false; element.children[i].style.cssText = "color: " + unify_color(style.color, false) + "!important"; } /*{ invert = false;  }*/ 
      else if (get_hue(style.color) == true && dark_bg == true)  { invert = true; element.children[i].style.cssText = "color: " + unify_color(style.color, true) + "!important";  } /*{ invert = true;   }*/ 
      //element.children[i].style.cssText = "color: " + unify_color(style.color, invert) + "!important"; 
    }
    if (style.backgroundColor != "rgba(0, 0, 0, 0)") {
      element.children[i].style.cssText = "background-color: " + unify_color(style.backgroundColor, false) + "!important"; 
      dark_bg = get_hue(style.backgroundColor);
    }
    //else if(style.backgroundColor == "rgba(0, 0, 0, 0)") {
      //adjust_to_background_color(element.children[i]);
    //}
    /*if (style.borderColor != "rgb(35, 38, 41)") {
      element.children[i].style.cssText = "border-right-color: " + unify_color(style.borderColor) + "!important";
}*/

    get_children(element.children[i]);
  }
}

get_children(document);

